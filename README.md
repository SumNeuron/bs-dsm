# Bootstrap Extras

This module "bsDSM" is a collection of small functions that may be of use for
any bootstrap based web-product.


# Functions

## Alerts

- makeBootstrapAlert

### makeBootstrapAlert

```javascript
bsDSM.makeBootstrapAlert(alertContainerId, title, message, style, alertId, customClass)
```

will result in the div:

```html
<div class="alert alert-dismissible fade show alert-<style> <customClass>" id="<alertID>">
  <strong>title: </strong> message <button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
```

being appended to `alertContainerId`.

## Nav-Bars

- smoothScrollSpy

### smoothScrollSpy

extends Bootstrap's scrollspy.

```javascript
ssSpy = bsDSM.smoothScrollSpy(navbarSelector, offsetSelector)

// set transition duration
// ssSpy.transitionDuration(<duration>)
// set offset value
// ssSpy.offset(<offset-number>)
```

Binds an animation to scroll to the location of the scrollSpy href anchor to the location. If offsetSelector is supplied will add the offset to this (e.g. if using
sticky header).
