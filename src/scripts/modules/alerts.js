export default function makeBootstrapAlert(
  alertContainerId,
  title,
  message,
  style,
  alertId,
  customClass
) {
  customClass = customClass == undefined ? '' : customClass;
  style = style == undefined ? 'secondary' : style

  var
  parent = d3.select(alertContainerId),
  alertDiv = parent.append('div')
  .classed('alert alert-dismissible fade show', true)
  .classed('alert-'+style, true)
  .classed(customClass, true),
  strong = alertDiv.append('strong').html(title + ': ')


  alertDiv.html( alertDiv.html() + message)
  if (alertId != undefined) {alertDiv.attr('id', alertId)}

  alertDiv.append('button')
  .attr('type', 'button')
  .attr('class', 'close')
  .attr('data-dismiss', 'alert')
  .html('&times;')

  return alertDiv
}
