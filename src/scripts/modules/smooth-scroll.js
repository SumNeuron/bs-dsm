export default function smoothScrollSpy(navbarSelector, offsetSelector) {
  var
  transitionDuration = 1000,
  offset = offsetSelector == undefined ? 0 : $(offsetSelector).outerHeight(),
  setHashQ = false

  ssSpy.transitionDuration = function(_){return arguments.length ? (transitionDuration = _, ssSpy) : transitionDuration}
  ssSpy.offset = function(_){return arguments.length ? (offset = _, ssSpy) : offset}
  ssSpy.setHashQ = function(_){return arguments.length ? (setHashQ = _, ssSpy) : setHashQ}

  function ssSpy(){
    $(navbarSelector + " ul li a[href^='#']").on('click', function(event){
      // prevent default anchor click behavior
      event.preventDefault();
      // store hash
      var hash = this.hash;
      // animate
      animateScrollTo(hash, transitionDuration)
    });
  }

  function correctHashOffsetOnLoad() {
    setOffset()
    var hash = location.hash;
    if ( hash != '' ) {animateScrollTo(hash, 0) }
  }

  function animateScrollTo(hash, duration) {
    setOffset()

    $('html, body').animate({
      scrollTop: $(hash).offset().top - offset
    }, duration, function() {
      if (history.pushState) {
        if (setHashQ) {history.pushState(null, null, hash)}
      }
      else {
        if (setHashQ) {location.hash = hash }
      }
    });
  }
  function setOffset() {
    offset = offsetSelector == undefined
    ? 0
    : $(offsetSelector).outerHeight() == undefined
      ? 0
      : $(offsetSelector).outerHeight()
  }

  correctHashOffsetOnLoad()
  return ssSpy
}
