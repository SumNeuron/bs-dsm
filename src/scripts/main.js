// import * as d3 from "d3";

// // Import a logger for easier debugging.
// import debug from 'debug';
// const log = debug('app:log');
//
// // The logger should only be disabled if we’re not in production.
// if (ENV !== 'production') {
//
//   // Enable the logger.
//   debug.enable('*');
//   log('Logging is enabled!');
// } else {
//   debug.disable();
// }

bsDSM = {}
import smoothScrollSpy from './modules/smooth-scroll'
import makeBootstrapAlert from './modules/alerts'

bsDSM.smoothScrollSpy = smoothScrollSpy;
bsDSM.makeBootstrapAlert = makeBootstrapAlert;

export default bsDSM
